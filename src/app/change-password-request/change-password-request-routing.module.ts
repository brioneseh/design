import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChangePasswordRequestPage } from './change-password-request.page';

const routes: Routes = [
  {
    path: '',
    component: ChangePasswordRequestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChangePasswordRequestPageRoutingModule {}
