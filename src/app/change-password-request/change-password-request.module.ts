import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChangePasswordRequestPageRoutingModule } from './change-password-request-routing.module';

import { ChangePasswordRequestPage } from './change-password-request.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChangePasswordRequestPageRoutingModule
  ],
  declarations: [ChangePasswordRequestPage]
})
export class ChangePasswordRequestPageModule {}
