import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChangePasswordRequestPage } from './change-password-request.page';

describe('ChangePasswordRequestPage', () => {
  let component: ChangePasswordRequestPage;
  let fixture: ComponentFixture<ChangePasswordRequestPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ChangePasswordRequestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
