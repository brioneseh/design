import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {
  mostrarContrasena: boolean = false; //Para mostrar o no la pass

  constructor() { }

  ngOnInit() {
  }
  toggleMostrarContrasena() {
    this.mostrarContrasena = !this.mostrarContrasena;
}

}
